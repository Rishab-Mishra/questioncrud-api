package org.o7planning.sbsecurity.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.o7planning.sbsecurity.dao.QuestionDAO;
import org.o7planning.sbsecurity.dao.TestAssignmentDAO;
import org.o7planning.sbsecurity.dao.studentInterface;
import org.o7planning.sbsecurity.dao.studentdao;
import org.o7planning.sbsecurity.entity.TestAssignment;
import org.o7planning.sbsecurity.entity.model.Input;
import org.o7planning.sbsecurity.entity.model.Question;
import org.o7planning.sbsecurity.utils.Token;
import org.o7planning.sbsecurity.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//@RestController
@Controller
public class MainController {

	@Autowired
	TestAssignmentDAO testAssignment;

	@Autowired
	QuestionDAO si;

	@RequestMapping(value = { "/", "/welcome", "/login" }, method = RequestMethod.GET)
	public String welcomePage(Model model) {
		model.addAttribute("title", "Welcome");

		return "loginPage";
	}

	@CrossOrigin
	@RequestMapping(value = "/getQuestions", method = RequestMethod.POST)
	public @ResponseBody String retrieveAllCarDetails(@RequestBody Input input) {

		List<Question> questionList = si.getAllQuestions(input.getType());

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final ObjectMapper mapper = new ObjectMapper();

		try {
			mapper.writeValue(out, questionList);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		final byte[] data = out.toByteArray();
		return new String(data);
	}

	@CrossOrigin
	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public @ResponseBody String test(@RequestBody Input input) {

		return input.getType() + "none";
	}

	@CrossOrigin
	@RequestMapping(value = "/addQuestion", method = RequestMethod.POST)
	public @ResponseBody int addQuestion(@RequestBody Question question) {

		return si.addQuestion(question);
	}

	@CrossOrigin
	@RequestMapping(value = "/modifyQuestion", method = RequestMethod.POST)
	public @ResponseBody int modifyQuestion(@RequestBody Question question) {

		return si.modifyQuestion(question);
	}

	@CrossOrigin
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPage(Model model, Principal principal) {

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		model.addAttribute("userInfo", userInfo);

		return "adminPage";
	}

	@RequestMapping(value = "/candidate", method = RequestMethod.GET)
	public String candidatePage(Model model, Principal principal) {

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		model.addAttribute("userInfo", userInfo);
		model.addAttribute("user", loginedUser);

//        List<Question> questionList=si.getAllQuestions();
//        System.out.println(questionList.get(0).toString());
		// System.out.println("s "+loginedUser.getUsername());
		List<TestAssignment> tests = testAssignment.getTests(loginedUser.getUsername());
		// System.out.println(testAssignment.getAllTests());
		model.addAttribute("tests", tests);
		return "candidatePage";
	}

	@RequestMapping(value = "/reviewer", method = RequestMethod.GET)
	public String reviewerPage(Model model, Principal principal) {

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		model.addAttribute("userInfo", userInfo);

		return "reviewerPage";
	}
// 
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String loginPage(Model model) {
// 
//        return "loginPage";
//    }
////    
//    @RequestMapping(value = "/register", method = RequestMethod.GET)
//    public String registerPage(Model model) {
// 
//        return "registerPage";
//    }
// 

	@CrossOrigin
	@RequestMapping(value = "/genTokens", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody List<String> genTokens(@RequestBody List<Token> users) {
		List<String> tokens = new ArrayList<>();
		for (Token user : users)
			try {
				tokens.add(user.genToken());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return tokens;
	}

	@RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	public String logoutSuccessfulPage(Model model) {
		model.addAttribute("logout", "true");

		return "loginPage";
	}

// 
	@RequestMapping(value = "/userInfo", method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {

		// After user login successfully.
		String userName = principal.getName();

		System.out.println("User Name: " + userName);

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		System.out.println(userInfo);
		model.addAttribute("userInfo", userInfo);
		if (userInfo.contains("ROLE_CANDIDATE"))
			return "forward:/candidate";
		if (userInfo.contains("ROLE_ADMIN"))
			return "forward:/admin";
		if (userInfo.contains("ROLE_REVIEWER"))
			return "forward:/reviewer";
		return "userInfoPage";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(Model model, Principal principal) {

		if (principal != null) {
			User loginedUser = (User) ((Authentication) principal).getPrincipal();

			String userInfo = WebUtils.toString(loginedUser);

			model.addAttribute("userInfo", userInfo);

			String message = "Hi " + principal.getName() //
					+ "<br> You do not have permission to access this page!";
			model.addAttribute("message", message);

		}

		return "403Page";
	}

	// Route to start the test
//    @PostMapping("/startTest")
//    public String startTest(Model model,@RequestParam(name = "testId") String testId) {
//    	System.out.println(model);
//    	  //return "exam_module/layout";
//    	model.addAttribute("test", testId);
//    	
//    	return "startTest";
//    }
//    
//    @CrossOrigin
//    @PostMapping("/getQuestions")
//    public String getQuestions(Model model,@RequestParam String type) {
//    	List<Question> questionList=si.getAllQuestions(type);
//    	return "startTest";
//    }
//
//    //Route to start the test
//    @PostMapping("/Instructions")
//    public String instructionsForTest(Model model,@RequestParam(name = "testId") String testId) {
//    	System.out.println(model);
//    	  //return "exam_module/layout";
//    	model.addAttribute("test", testId);
//    	return "instructions";
//    }

//    @PostMapping("/test")
//    public String candidateTest(Model model,@RequestParam(name = "testId") String testId) {
//    	 List<Question> questionList=si.getAllQuestions();
//    	 Collections.shuffle(questionList,new Random());
//    	 int i=1;
//		 for(Question question:questionList) {
//			 question.setQuestionNo(""+i);
//			 i++;
//		 }
//         final ByteArrayOutputStream out = new ByteArrayOutputStream();
//         final ObjectMapper mapper = new ObjectMapper();
//
//         try {
// 			mapper.writeValue(out, questionList);
// 		} catch (JsonGenerationException e) {
// 			// TODO Auto-generated catch block
// 			e.printStackTrace();
// 		} catch (JsonMappingException e) {
// 			// TODO Auto-generated catch block
// 			e.printStackTrace();
// 		} catch (IOException e) {
// 			// TODO Auto-generated catch block
// 			e.printStackTrace();
// 		}
//
//         final byte[] data = out.toByteArray();
//         model.addAttribute("data", new String(data));
//         System.out.println(new String(data));
//         TestAssignment test=testAssignment.getTest(testId);
//         switch(test.getTestType()) {
//         case "F":
//             model.addAttribute("time",40*60);
//        	 break;
//         case "E":
//             model.addAttribute("time",70*60);
//        	 break;
//         }
//         model.addAttribute("test", testId);
//         return "exam_module/layout";
//    }
//    
//    @PostMapping("/endTest")
//    public String endTest(Model model,@RequestParam(name = "testId") String testId,@RequestParam(name = "user_score") String user_score) {
//    	//System.out.println(model);
//    	  //return "exam_module/layout";
//    	//model.addAttribute("score", score);
//    	System.out.println("score is "+user_score);
//    	System.out.println("test score is "+testId);
//    	model.addAttribute("score",user_score);
//    	//testAssignment.
//    	return "endTest";
//    }
}