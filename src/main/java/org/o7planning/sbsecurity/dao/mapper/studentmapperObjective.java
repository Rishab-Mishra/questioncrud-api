package org.o7planning.sbsecurity.dao.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import org.o7planning.sbsecurity.entity.model.*;


public class studentmapperObjective implements RowMapper {

	public Objective mapRow(ResultSet rs, int rowNum) throws SQLException {
	ArrayList<String> answers=new ArrayList<String>();
	answers.add(rs.getString("OP1"));
	answers.add(rs.getString("OP2"));
	answers.add(rs.getString("OP3"));
	answers.add(rs.getString("OP4"));
	
	String db_ans=rs.getString("AOP");
	int index=answers.indexOf((String)db_ans); 
	Objective o=new Objective("objective",rs.getInt("OB_ID"),rs.getString("QUESTION"),rs.getInt("SEC_ID"),answers,"",String.valueOf(index+1));
	return o;
	}
}